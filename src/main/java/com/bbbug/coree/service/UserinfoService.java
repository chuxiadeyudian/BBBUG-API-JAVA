package com.bbbug.coree.service;

import com.bbbug.coree.entity.Userinfo;
import com.bbbug.coree.mapper.UserinfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service

public class UserinfoService {
    @Resource
    UserinfoMapper mapper;
    public Userinfo queryById(Integer userId){
        return mapper.queryById(userId);
    }
    public List<Userinfo> queryAllByLimit(int offset, int limit){
        return mapper.queryAllByLimit(offset,limit);
    }
    public Userinfo queryByAccount(String userAccount){
        return mapper.queryByAccount(userAccount);
    }
    public Userinfo queryByBoth(String account,String password){
        return mapper.queryByBoth(account,password);
    }
    public Boolean insert(Userinfo userinfo){
        return mapper.insert(userinfo);
    }
    public Boolean update(Userinfo userinfo){
        return mapper.update(userinfo);
    }
    public boolean deleteById(Integer userId){
        return mapper.deleteById(userId);
    }
    public Userinfo queryBythrid(String thridid){
        return mapper.queryBythrid(thridid);
    }
}
